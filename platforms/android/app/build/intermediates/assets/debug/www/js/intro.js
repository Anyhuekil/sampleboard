var intro = (function(root){
    $("#intro_sample_logo").on("click", function(){
    	$("#intro").css({"display":"none"});
    	$("nav").css({"display":"none"});
        $("header").css({"display":"block"});
        $("footer").css({"display":"block"});
        $("section").css({"display":"block"});
        $("section").css({"display":"block"});
        
        $("header").load("page/header.html");
        $("section").load("page/main.html");
    });
})(this);
