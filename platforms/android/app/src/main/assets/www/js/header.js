(function(){
	$("#header_search").on("click", function(){
		$("#header_search_input").focus();
		$("#header_search_input").css({"width":"250px", "padding":"0px 40px 0px 10px"});
		$("#header_search_icon").css({"border":"1px solid white","border-radius":"0px 5px 5px 0px", "background":"#006DF0"});
	});
	$("#header_search").on("focusout", function(){
		$("#header_search_input").css({"width":"0px", "padding":"0px"});
		$("#header_search_icon").css({"border":"0px","border-radius":"5px", "background":"white"});
	});
})();